const saveStrategy = (strategyName, targets) => {
    let newTargets = [...targets];

    while (newTargets.length < 10) {
        newTargets.push({percentage: 0});
    }

    console.log(strategyName, newTargets);
    // post to backend with strategyName and newTargets
};

const backendService = {
    saveStrategy
};

export default backendService;