import React, { useState, useCallback, useMemo, useEffect } from 'react';
import Proptypes from 'prop-types';
import Modal from 'react-modal';
import Target from './Target';
import { Input } from '../components';
import backendService from '../backendService';
import './strategy-modal.css';

Modal.setAppElement('#root');

const MAX_NAME_LENGTH = 64;
const MAX_TARGETS = 10;
const initialTargets = [{percentage: 25}, {percentage: 25}, {percentage: 25}, {percentage: 25}];

const StrategyModal = (props) => {
    const {isOpen} = props;
    const {closeModal} = props;
    const [strategyName, setStrategyName] = useState('');
    const [targets, setTargets] = useState(initialTargets);

    useEffect(() => {
        setTargets(initialTargets);
        setStrategyName('');
    }, [isOpen]);

    const createNewTarget = () => {
        if (targets.length < MAX_TARGETS) {
            const newTargets = [...targets, {percentage: 0}];
            setTargets(newTargets);
        }
    };

    const deleteTarget = useCallback((targetIndex) => {
        const newTargets = [...targets];
        newTargets.splice(targetIndex, 1);
        setTargets(newTargets);
    }, [targets]);

    const deleteLastTarget = useCallback(() => {
        if (targets.length > 0) {
            deleteTarget(targets.length - 1);
        }
    }, [targets, deleteTarget]);

    const updateTargetPercentage = useCallback((targetIndex, percentage) => {
        const newTargets = [...targets];
        newTargets[targetIndex].percentage = percentage;
        setTargets(newTargets);
    }, [targets]);

    const saveStrategy = useCallback(() => {
        backendService.saveStrategy(strategyName, targets);
        closeModal();
    }, [strategyName, targets, closeModal]);

    const percentageSum = useMemo(() => (
        targets.reduce((sum, current) => (
            sum + current.percentage)
        , 0)
    ), [targets]);

    const isValidated = useMemo(() => {
        const isNameValid = strategyName && strategyName.length <= MAX_NAME_LENGTH;

        return percentageSum === 100 && isNameValid;
    }, [percentageSum, strategyName]);

    return (
        <Modal
            isOpen={isOpen}
            className="strategy-modal"
        >
            <div className="modal-header">
                <h2>Entry</h2>
                <div onClick={closeModal}>X</div>
            </div>

            <div className="modal-body">
                <Input
                    label="Strategy Name:"
                    value={strategyName}
                    onChange={setStrategyName}
                />

                <div className="targets">
                    <div className="targets-title">
                        <span>{targets.length} Targets</span>

                        <div className="targets-buttons">
                            <button onClick={createNewTarget}>+</button>
                            <button onClick={deleteLastTarget}>-</button>
                        </div>
                    </div>

                    {
                        targets.map(({percentage}, index) => (
                            <Target
                                key={index}
                                index={index}
                                percentage={percentage}
                                updatePercentage={updateTargetPercentage}
                                deleteTarget={deleteTarget}
                            />
                        ))
                    }

                    <div className="targets-total">
                        <span>Total</span>
                        <span>{percentageSum}%</span>
                    </div>
                </div>
            </div>

            <div className="modal-footer">
                <button onClick={closeModal}>Cancel</button>
                <button disabled={!isValidated} onClick={saveStrategy}>Save</button>
            </div>
        </Modal>
    );
};

StrategyModal.propTypes = {
    isOpen: Proptypes.bool,
    closeModal: Proptypes.func
}

StrategyModal.defaultProps = {
    isOpen: false,
    closeModal: () => {}
};

export default StrategyModal;