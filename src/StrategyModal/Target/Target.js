import React from 'react';
import PropTypes from 'prop-types';
import { Input } from '../../components';
import './target.css';

const Target = ({index, percentage, deleteTarget, updatePercentage}) => {
    const innerDeleteTarget = () => {
        deleteTarget(index);
    }

    const innerUpdatePercentage = (newPercentage) => {
        const parsedPercentage = parseInt(newPercentage);
        updatePercentage(index, parsedPercentage);
    }

    return (
        <div className="target">
            <div onClick={innerDeleteTarget}>X</div>
            <span>Target {index + 1}</span>

            {
                percentage > 100
                ? 'percentage cant be more than 100'
                : ''
            }

            <Input onChange={innerUpdatePercentage} value={percentage} type="number" />
        </div>
    );
};

Target.propTypes = {
    index: PropTypes.number,
    percentage: PropTypes.number,
    deleteTarget: PropTypes.func,
    updatePercentage: PropTypes.func
}

export default Target;