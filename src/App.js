import { useState } from 'react';
import StrategyModal from './StrategyModal';
import logo from './logo.svg';
import './App.css';

function App() {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const openModal = () => {
        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <button
                    className="App-link"
                    onClick={openModal}
                >
                    Open Modal
                </button>
            </header>

            <StrategyModal
                isOpen={isModalOpen}
                closeModal={closeModal}
            />
        </div>
    );
}

export default App;
