import React from 'react';
import PropTypes from 'prop-types';
import './input.css';

const Input = ({label, value, onChange, type}) => {
    const innerOnChage = ({target}) => {
        onChange(target.value);
    };

    return (
        <div className="input">
            <label>{label}</label>
            <input type={type} value={value} onChange={innerOnChage} />
        </div>
    );
};

Input.propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
    onChange: PropTypes.func,
    type: PropTypes.string
}

Input.defaultProps = {
  type: 'text'
};

export default Input;